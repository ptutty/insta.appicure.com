(function($){

// view for menu once user has signed into to instagram 
App.Views.MenuAuth = Backbone.View.extend({ // perhaps bind this to a new menu model at some point?
	tagName: "nav",
	id: "controls",

	initialize: function() {
		_.bindAll(this, 'render', 'bytag' ,'events' , 'controls');
		this.template = _.template($('#auth-menu-tmpl').html());
		this.render();
		
	},
	events: { // using routes rather than events for the web app - 
		"click #logout" : "logout",
		"click #searchButton" : "search",
		"click #likethisphoto" : "likethisphoto",
	},
	
	render: function(){
		$(this.el).html(this.template());
		var $menu = $("#menu");
		$menu.empty();
		$menu.append(this.el);
		// return this;
	},
	
	
	feed: function(){
		App.Main.display("users/self/feed");	
	},
	
	search: function(e){
		var $checked = $("input[name='searchtype']:checked")
		var searchfor = $checked.val();
		var query = $("#search").val();
		switch(searchfor) {
			case "tag":
			App.router.navigate("/tagsearch/" + query, {trigger: true});
			break;
			
			case "user":
			App.router.navigate("/usersearch/" + query, {trigger: true});
			break;
			
			case "location":
			console.log(query);
			App.Helpers.geoLookup(query);
			break;
			
		}
	},
	
	
	popular: function(){
		App.Main.display("media/popular");
	},
	
	
	logout: function(){
		App.Helpers.eraseCookie('access_token');
		App.settings.accesstoken = null;
		window.location = "#/logout/";
		
		
		return false;
	},
	
	likethisphoto : function(){
		console.log("like this photo");
	}
	
	
	
})




// view for default menu
App.Views.MenuDefault = Backbone.View.extend({
	tagName: "nav",
	id: "controls",
	initialize: function() {
		_.bindAll(this, 'render');
		this.template = _.template($('#default-menu-tmpl').html());
		this.render();
		
	},
	
	render: function(){
		$(this.el).html(this.template({}));
		
		$menu = $("#menu");
		$menu.empty();
		$menu.append(this.el);
		// return this;
		
	},
	
	events: {
		"click #popularLink"  : "popular",
	},
	
	
	popular: function(){
		// add code to refresh popular images.
	}
	
	
})



// view for footer when authenticated.

App.Views.FooterAuth = Backbone.View.extend({
	tagName: "div",
	id: "currentUser",
	initialize: function() {
		_.bindAll(this, 'render');
		this.template = _.template($('#current-user-template').html());
		this.render();
		
	},
	
	render: function(){
		$(this.el).html(this.template({}));
		
		$footer = $("#footer");
		// $menu.empty();
		$footer.append(this.el);
		// return this;
		
	},
	
	events: {
		// "click #popularLink"  : "popular",
	},
	
})



})(jQuery);