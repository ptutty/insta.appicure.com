
// the main controller class.
App.Main = {
	
	
	// controls display of body of page
	/* basic usage:
	
		1. restful endpoint is supplied to method as a parameter 
		2. case statement decides what logic so perform with paramter
		3. basic logic is: 
			a. check if view already exist - basic caching
			b. create a new collection based on collection class -supply it with endpoint
			c. collection class requests JSON data and returns a collection (array of models) 
			d. on successful return of collection a new view is created from appropriate view class.
			c. collection is suppllied to view for display. 
	
	*/
	
	
	display: function(endpoint, mediaID, userID, query) {
		$(".simplebutton").removeClass("active");
		
		switch(endpoint) {
				case "media/popular":
				
				if (App.views.popular) { // checks if view already exists
					App.views.popular.render(); // if view already exists just render it.
				} else { // view does not already exist. Create collection, view , fetch data.
					App.collections.popular = new App.Collections.Users([],{query: endpoint + "?"});  // see js/collections/users.js
					App.collections.popular.fetch({success: function(users, response){
						App.views.popular = new App.Views.UserDisplay({collection: App.collections.popular}); // js/views/
					 	}
					 }); 
				}
				
				$("#popularLink").addClass("active");
	
				break;
				
				case "users/self/feed":
				
					if (App.views.feed) {
						App.views.feed.render();
					} else {
						App.collections.feed = new App.Collections.Users([],{query: endpoint + "?"}); 
						App.collections.feed.fetch({success: function(users, response){
							App.views.feed = new App.Views.UserDisplay({collection: App.collections.feed});
						}
					 	});
					}
				
					 $("#feedLink").addClass("active");
				
				break;
				
				case "users/self/media/recent/":
				
					if (App.views.mymedia) {
						App.views.mymedia.render();
					} else {
						App.collections.mymedia = new App.Collections.Users([],{query: endpoint + "?"}); 
						App.collections.mymedia.fetch({success: function(users, response){
							App.views.mymedia = new App.Views.UserDisplay({collection: App.collections.mymedia});
						}
					 	});
					}				
			
					$("#meLink").addClass("active");
					break;
				
				case "users/search":
					App.collections.usersearch = new App.Collections.Users([],{query: endpoint + "?q=" + query + "&"}); 
					App.collections.usersearch.fetch({success: function(users, response){
						App.views.usersearch = new App.Views.UserSearchDisplay({collection: App.collections.usersearch});
					}
					});
				break;
				
				case "tags":			
						App.collections.tags = new App.Collections.Users([],{query: endpoint + "/" + query + "/media/recent?"}); 
						App.collections.tags.fetch({success: function(users, response){
							App.views.tags = new App.Views.UserDisplay({collection: App.collections.tags});
						}
					 	});
				break;	
				
				
				case "locations":			
						App.collections.locations = new App.Collections.Users([],{query: endpoint + "/search?" + App.settings.latlng + "&"}); 
						App.collections.locations.fetch({success: function(users, response){
							App.views.locations = new App.Views.LocationSearchDisplay({collection: App.collections.locations});
						}
					 	});
				break;	
				
				case "users/self": // for info about current logged in user			
						App.collections.currentuser = new App.Collections.Users([],{query: endpoint + "?"}); 
						App.collections.currentuser.fetch({success: function(users, response){
							App.views.currentuser = new App.Views.CurrentUser({collection: App.collections.currentuser});
						}
					 	});
						
				break;	
				
				
				case "media": // for detailed info about media
						App.collections.mediadetail = new App.Collections.Users([],{query: endpoint + "/" + mediaID + "?"}); 
						App.collections.mediadetail.fetch({success: function(users, response){
							App.views.mediadetail = new App.Views.MediaDetail({collection: App.collections.mediadetail});
						}
					 	});
				break;	
				
				case "users": // for request /users/{user-id}/media/recent 
						App.collections.userrecent = new App.Collections.Users([],{query: endpoint + "/" + userID + "/media/recent?"}); 
						App.collections.userrecent.fetch({success: function(users, response){
							App.views.userrecent = new App.Views.UserDisplay({collection: App.collections.userrecent});
						}
					 	});
				break;	
				
				case "users/self/media/liked": // for requests /users/self/media/liked
						App.collections.userliked = new App.Collections.Users([],{query: endpoint + "?"}); 
						App.collections.userliked.fetch({success: function(users, response){
							App.views.userliked = new App.Views.UserDisplay({collection: App.collections.userliked});
						}
					 	});
						
						$("#likesLink").addClass("active");
				break;	
				
				
				
				
			
		}
				
	}
}
