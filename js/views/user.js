// this is where all the views for displaying collection data live.


// iterates over a collection, creates a new view per model and puts it in ul.
App.Views.UserDisplay = Backbone.View.extend({
	initialize: function(){	
			_.bindAll(this, 'render');
			this.render();		
	},
	
	render: function() {
		var $ul = $("<ul>").addClass("gallery");	
		this.collection.each(function(user, index) { // see main.js for collection that are supplied when instantiated.
			var item = new App.Views.User({model:user}); // create an instance of user view class for each model in collection
			var renderedContent = item.render().el; // call render method on instance and assign to var
			$ul.append(renderedContent); // append var to JQ collection
		  })
		$container = $("#container");
		$container.empty();
		$container.append($ul); // append JQ collection to dom element 
    }
})

// basic user model (user = photo)
App.Views.User = Backbone.View.extend({
 	tagName: 'li',
	className: 'user_img',
 
	initialize: function(){	
			_.bindAll(this, 'render');
			this.template = _.template($('#caption-user-tmpl').html());
			this.render();		
	},
  
    render: function() {	
		this.renderedContent = this.template(this.model.toJSON());
		$(this.el).html(this.renderedContent);
			
			// rollover caption effect setup
			$caption = $(this.el).find('div.description');
			$caption.css('opacity', 0)  
					//..set width same as the image...  
				.css('width', $caption.siblings('img').width())  
				.parent().css('width', $caption.siblings('img').width())  
				.css('display', 'block');  
				return this; 	
    },
	
	events: {
			"mouseenter div.wrapper"   : "hoverOn",
			"mouseleave div.wrapper"   : "hoverOff",
		},
		
		
	hoverOn: function(e) { 
		//when mouse hover over the wrapper div  
				//get it's children elements with class description '  
				//and show it using fadeTo 
				$(this.el).find('div.description').stop().fadeTo(500, 0.7); 
			},
		
		
		hoverOff: function(e) { //when mouse out of the wrapper div  
				//use fadeTo to hide the div  
				$(this.el).find('div.description').stop().fadeTo(500, 0);
				}	
		
});

// user search results
App.Views.UserSearchDisplay = Backbone.View.extend({
	initialize: function(){	
			_.bindAll(this, 'render');
			this.render();		
	},
	
	render: function() {
		var $ul = $("<ul>").addClass("gallery");	
		this.collection.each(function(user, index) {
			var item = new App.Views.UserSearch({model:user});
			var renderedContent = item.render().el;
			$ul.append(renderedContent);
		  })
		$container = $("#container");
		$container.empty();
		$container.append($ul);
    }
})


// for user search results
App.Views.UserSearch = App.Views.User.extend({
	initialize: function(){	
			_.bindAll(this, 'render');
			this.template = _.template($('#search-user-tmpl').html());
			this.render();		
	},
	events: {
			"click" : "user_recent"
		},
		
	user_recent: function(){
		var userID = this.model.get("id");
		App.Main.display("users" , null, userID);	
	}
		
});

// location search display
App.Views.LocationSearchDisplay = Backbone.View.extend({
	initialize: function(){	
			_.bindAll(this, 'render');
			this.render();		
	},
	
	render: function() {
		var $ul = $("<ul>");	
		this.collection.each(function(user, index) {
			var item = new App.Views.LocationSearch({model:user});
			var renderedContent = item.render().el;
			$ul.append(renderedContent);
		  })
		$container = $("#container");
		$container.empty();
		$container.append($ul);
    }
})

// for user search results
App.Views.LocationSearch = App.Views.User.extend({
	initialize: function(){	
			_.bindAll(this, 'render');
			this.template = _.template($('#location-user-template').html());
			this.render();		
	},
	event: {
	 "click": "recent_user"	
	},
	
	
	recent_user: function() {
			var user_id = this.model.get("id");	
			App.Main.display("users",null, user_id);
	}
});

// current logged in user displayed in the footer.
App.Views.CurrentUser = Backbone.View.extend({ 
	tagName: "div",
	id: "currentUser",

	initialize: function() {
		_.bindAll(this, 'render');
		this.template = _.template($('#current-user-template').html());
		this.template_welcome = _.template($('#modal-instruction-auth-template').html()); // welcome dialog
		this.render();	
	},
	
	render: function() {
		var collection = this.collection;
			this.renderedContent = this.template(collection.models[0].toJSON())
			$(this.el).html(this.renderedContent);
			$("#footer").append(this.el);
			var $welcome = this.template_welcome(collection.models[0].toJSON());
			$.colorbox({inline:true, width:"50%", open:true, href: $welcome, opacity: 0.1});
			
			return this;
	},
	
});


// display details of a photo
App.Views.MediaDetail = Backbone.View.extend({
	
		tagName: 'section',
		id: 'main',
		initialize: function(){
			_.bindAll(this, 'render', 'events');
			this.template = _.template($('#media-detail-template').html());
			this.model = this.collection.models[0].toJSON();
			if (this.model.location) {
				if (this.model.location.latitude){ 
				App.Helpers.reverseGeo(this.model.location);
				}
			} else {
				App.settings.address = null;
				this.render();	
			}
			
					 
		},
		
		events: {
			"click #likethisphoto" : "like",
			"click #followthisuser" : "follow",
			
		},
	
		render: function() {
			this.renderedContent = this.template(this.model);
			$(this.el).html(this.renderedContent);
			
			$container = $("#container");
			$container.empty();
			$container.append(this.el);
			return this; 	
		},
		
		like: function() {
			var data = "url=https://api.instagram.com/v1/media/" + this.model.id + "/likes&access_token=" + App.settings.accesstoken; 
			App.Helpers.IGPost(data);
			return false;
		},
		
		follow: function(){
			var data = "url=https://api.instagram.com/v1/users/" + this.model.user.id + "/relationship?access_token=" + App.settings.accesstoken + "&action=follow"; 
			App.Helpers.IGPost(data);
			return false;
		}
	})
