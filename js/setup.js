
// routes.js bootstraps this whole app!
// this class is instantiated in index.html and checks prevous logged in status via cookie once instantiation.


App.Views.InstaView = Backbone.View.extend({ 
	initialize: function() {
			this.cookiecheck();
	},
	
	
	render: function(){ // this method is called from routes.js(line 22) and returns a value to it which decides the main content view.  
	
		// if access token variable is set
		if (App.settings.accesstoken){ // logged in
			
			if (!this.header_auth) { // check to see if logged in menu view class has already been instantiated.
				this.header_auth = new App.Views.MenuAuth(); // show full menu. see menu.js
				App.Main.display("users/self"); // display current user info in footer
			}
			
			return true;
			
		} else { // not logged in
			if (!this.header_noauth) {
				this.header_noauth = new App.Views.MenuDefault(); // display log in menu. see menu.js
			}
				
			var $welcome  = _.template($('#modal-instruction-unauth-template').html());
			$.colorbox({inline:true, width:"50%", open:true, href: $welcome, opacity: 0.1});
			return false;
			}; 
	
	// modal instructions	
	
		
	},
	
	cookiecheck: function(){
		var accesstoken = App.Helpers.readCookie('access_token'); // see app.js
		if (accesstoken){ // cookie set
			App.settings.accesstoken = accesstoken;	 // set accesstoken variable
		}
	}
	
});	


