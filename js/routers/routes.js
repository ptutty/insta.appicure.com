
/* routers */
// this is the main controller, along with main.js. It bootstraps the whole app on line 22.    


App.Routers.Routes = Backbone.Router.extend({
		routes: {
			'': 'home',
			':id' : 'auth',
			'/mediadetail/:id': 'mediadetail',
			'/user/:id': 'userdetail',
			'/myphotos/' : 'myphotos',
			'/popular/' : 'popular',
			'/feed/' : 'feed',
			'/tagsearch/:id': 'tagsearch',
			'/usersearch/:id': 'usersearch',
			'/locationsearch/:id': 'locationsearch'	,
			'/myliked/': 'liked',
			'/logout/': 'logout'			
		},
		
		home: function() {
			if (App.insta.render()) { // this line bootstrap the whole app! See setup.js. 
				App.router.navigate("/feed/", {trigger: true}); // user logged in so display their feed
			} else {
				App.router.navigate("/popular/", {trigger: true}); // user not logged in yets displat popular images	
			}
			
		},
		
		auth: function(id) {
			var access_token = App.Helpers.getaccessToken(id);
			App.Helpers.createCookie('access_token',access_token,14);
			App.insta.cookiecheck();
			if (App.insta.render()){
				App.Main.display("users/self/feed");
				App.router.navigate("/feed/", {trigger: false});	
			}
		},
		
		mediadetail: function(id) {
			App.insta.render();
			App.Main.display("media" , id);
		},
		
		userdetail: function(id) {
			if (App.insta.render()){
				App.Main.display("users",null,id);
			}
			
		},
		
		myphotos: function(){
			if (App.insta.render()){
				App.Main.display("users/self/media/recent/");
			}
		},
		
		popular: function(){
				App.insta.render();
				App.Main.display("media/popular"); // see main.js
		},
		
		feed: function() {
			if (App.insta.render()) {  // returns true if user is logged in.
				App.Main.display("users/self/feed");
			} else {
				App.router.navigate("/popular/", {trigger: true});
			}
		},
		
		tagsearch: function(id) {
			if (App.insta.render()) {  // returns true if user is logged in.
				App.Main.display("tags", null, null, id);
			}
		},
		
		usersearch: function(id){
				if (App.insta.render()) {  // returns true if user is logged in.
					App.Main.display("users/search" , null, null, id);
				}
		},
		
		liked: function(){
			if (App.insta.render()){;
				App.Main.display("users/self/media/liked");
			}
			
		},
		
		logout: function(){
			var $iframe = '<iframe src="https://instagram.com/accounts/logout/" width="0" height="0">'
			$('#hidden').html($iframe);
			App.router.navigate("/popular/", {trigger: true}); // user not logged in yets displat popular images	
		}
	})

App.router = new App.Routers.Routes();